$(function () {
                $('[data-toggle="popover"]').popover();
                $('.popover-dismiss').popover({
                     trigger: 'focus'
                    });

                $('.carousel').carousel({
                    interval: 5000
                });

                $('#contact').on('show.bs.modal', function(e){
                    console.log('El modal se está mostrando');
                    $('#contactoBtn').removeClass('btn-outline-success');
                    $('#contactoBtn').addClass('btn-primary');
                    $('#contactoBtn').prop('disabled', true);
                });

                $('#contact').on('shown.bs.modal', function(e){
                    console.log('El modal contacto se mostró');
                });

                $('#contact').on('hide.bs.modal', function(e){
                    console.log('El modal se está ocultando');
                });

                $('#contact').on('hidden.bs.modal', function(e){
                    console.log('El modal contacto se ocultó');
                    $('#contactoBtn').removeClass('btn-primary');
                    $('#contactoBtn').addClass('btn-outline-success');
                    $('#contactoBtn').prop('disabled', false);
                });
            });